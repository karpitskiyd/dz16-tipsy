
import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var billTextField: UITextField!
    @IBOutlet weak var zeroPctButtom: UIButton!
    @IBOutlet weak var tenPctButtom: UIButton!
    @IBOutlet weak var twentyPctButtom: UIButton!
    @IBOutlet weak var splitNumberLabel: UILabel!
    
    var currentTips = 0.0
    var guestsAmount = 2
    var currentCheck = 0.0
    var checkOut = 0.0
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        zeroPctButtom.isSelected = true
        tenPctButtom.isSelected = false
        twentyPctButtom.isSelected = false
    }
    
    
    @IBAction func tipChanged(_ sender: UIButton) {
        zeroPctButtom.isSelected = false
        tenPctButtom.isSelected = false
        twentyPctButtom.isSelected = false
        sender.isSelected = true
        let buttonTitle = sender.currentTitle!
        let titleWithoutPrecentage = String(buttonTitle.dropLast())
        currentTips = (Double(titleWithoutPrecentage)!)/100
        
    }
    
    @IBAction func stepperValueChanged(_ sender: UIStepper) {
        splitNumberLabel.text = String(format: "%.0f", sender.value)
        let convertSplit = splitNumberLabel.text!
        guestsAmount = Int(convertSplit)!
        
    }
    
    
    @IBAction func calculatePressed(_ sender: UIButton) {
        
        calculationProcess()
        performSegue(withIdentifier: "goToResult", sender: self)
    }
    
    func calculationProcess (){
        currentCheck = Double(billTextField.text ?? "0.0")!
        checkOut = currentCheck/Double(guestsAmount)
        print (checkOut)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToResult" {
            let destinationVC = segue.destination as! ResultsViewController
            destinationVC.finalCheck = String(format: "%.1f", checkOut)
            destinationVC.comments = "Split between \(guestsAmount) people, with \(String(format: "%.2f", checkOut * currentTips))$ tips."
    
    
        }
}

}
